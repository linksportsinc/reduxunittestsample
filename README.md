# ReduxUnitTestSample

[testing-react-redux-with-jest-for-Beginner](https://github.com/wasudev/testing-react-redux-with-jest-for-Beginner)を参考に、  
Jestのテストコードに対してコメントを追記しました。


## Coverage
package.jsonのscriptsにて、testコマンドにjest --coverageを登録することで、  
テスト結果のカバレッジが出力されます。

また、別途htmlでカバレッジを出力していますが、方法はわかっていません。  
coverage > lcov-report > index.html


## Qiitaまとめ記事
- [テスト概要](http://qiita.com/yuri_iOS/items/855609b35cecaeb94abf)
- [Jest準備と概要](http://qiita.com/yuri_iOS/items/9f14d1f1d9f41e37fe4b)
- [非同期処理のテストとモック](http://qiita.com/yuri_iOS/items/7003273ebedf77074d56)


## How to run test
    $ npm run test
    $ npm run test:watch (for watch mode)

    run web application:
    $ npm run dev

    run service:
    $ npm run json-server



