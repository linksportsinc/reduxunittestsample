import React from 'react'
import { shallow } from 'enzyme'

import CommentBox from '../CommentBox'

describe('[Unit] CommentBox Controller', () => {
  let component
  let props

  beforeEach(() => {
    props = {
      addPost: jest.fn(),
      typing: jest.fn(),
      message: '',
    }
    component = shallow(<CommentBox {...props} />)
  })

  /* inputコンポーネントの値に入力されてchangeアクションが発生した際に、typingに指定した関数が呼び出されることを確認するテスト */
  it('Should call fn after typing', () => {
    component.find('input').simulate('change', {
      target: {
        value: 'Hello',
      },
    })
    //関数[toHaveBeenCalled()]:モック関数が呼ばれたか判定
    expect(props.typing).toHaveBeenCalled()
  })

  /* inputコンポーネントに入力値がある状態でclickボタンを押下した際に、入力値を引数としてaddPostに指定した関数が呼ばれることをテスト*/
  it('Should call fn after click button', () => {
    component.setProps({ message: 'Hello' })
    component.find('button').simulate('click')
    // 関数[toHaveBeenCalledWith(args1,args2...)]:関数が引数とともに呼ばれたか判定
    expect(props.addPost).toHaveBeenCalledWith('Hello')
  })
})
